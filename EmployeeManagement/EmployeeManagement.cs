﻿using System.Globalization;
using Microsoft.Win32.SafeHandles;
using System.Security.Cryptography.X509Certificates;

public class EmployeeManagement
{
    Employee[] employees { get; set; }
    int count = 0;

    public EmployeeManagement()
    {
        employees = new Employee[100]; // Số nhân viên lưu trữ tối đa
    }
    public void SetEmployees(Employee[]? employees)
    {
        ArgumentNullException.ThrowIfNull(employees);
        this.employees = employees;
    }
        
    public Employee[] GetEmployees()
        => employees.Where(x => x is not null).ToArray();

    public void AddEmployee(string name, int age, string possition, decimal salary)
        => employees[count++] = new Employee()
        {
            Id = count,
            Name = name,
            Age = age,
            Position = possition,
            Salary = salary
        };

    public void RemoveEmployee(string name)
    {
        count--;
        employees = employees
            .Where(x => x is not null && string.Compare(name, x.Name, StringComparison.OrdinalIgnoreCase) != 0)
            .ToArray();
    }
}
