﻿using System.Text.Json;

public class EmployeeManagementUI
{
    public EmployeeManagement employeeManagement { get; set; }

    public EmployeeManagementUI()
    {
        // đối tượng này dùng để quản lý và lưu trữ nhân viên
        employeeManagement = new EmployeeManagement();
    }
    public void ReadFromFile()
    {
        System.Console.Write("Vui lòng nhập tên file: ");
        var fileName = Console.ReadLine() ?? string.Empty;
        fileName += ".json";
        if (File.Exists(fileName))
        {
            string content = File.ReadAllText(fileName);
            if (!string.IsNullOrEmpty(content))
            {
                employeeManagement.SetEmployees(JsonSerializer.Deserialize<Employee[]>(content));
            }
            else
            {
                System.Console.WriteLine("File trống hoặc đọc file không thành công");
            }
        } else 
        {
            System.Console.WriteLine("File không tồn tại.");
        }
    }
    public void SaveToFile()
    {
        System.Console.Write("Vui lòng nhập tên File: ");
        string fileName = Console.ReadLine() ?? string.Empty;
        fileName += ".json";
        if (File.Exists(fileName))
        {
            System.Console.Write("File đã tồn tại? Bạn muốn ghi đè không? (Y/N)");
            string confirm = System.Console.ReadLine() ?? string.Empty;
            if (confirm == "Y")
            {
                File.WriteAllText(path: fileName, JsonSerializer.Serialize(employeeManagement.GetEmployees()));
            }
        }
        else
        {
            File.WriteAllText(path: fileName, JsonSerializer.Serialize(employeeManagement.GetEmployees()));
        }

    }
    public void PrintEmployees()
    {

        Employee[] employees = employeeManagement.GetEmployees();
        foreach (var emp in employees)
        {
            Console.WriteLine(string.Format("Tên: {0, -15}, Tuổi: {1, -2}, Chức vụ: {2, -15}, Lương: {3, -6}", emp.Name, emp.Age, emp.Position, emp.Salary));
        }
        Console.WriteLine("Hiển thị thông tin của nhân viên");
    }

    public void AddEmployee()
    {
        // TODO: Yêu cầu người dùng nhập tên, tuổi, chức vụ và lương của nhân viên mới
        Console.WriteLine("Tạo một đối tượng Employee và thêm vào danh sách");
        // Tạo một đối tượng Employee và gọi hàm employeeManagement.AddEmployee();
        System.Console.Write("Vui lòng nhập tên: ");
        var name = Console.ReadLine() ?? string.Empty;
        System.Console.Write("Vui lòng nhập tuổi: ");
        int.TryParse(Console.ReadLine(), out int age);
        System.Console.Write("Vui lòng nhập chức vụ: ");
        var position = Console.ReadLine() ?? string.Empty;
        System.Console.Write("Vui lòng nhập chức lương: ");
        decimal.TryParse(Console.ReadLine(), out decimal salary);
        employeeManagement.AddEmployee(name: name,
            age: age,
            possition: position,
            salary: salary);

    }

    public void RemoveEmployee()
    {

        Console.Write("Vui lòng nhập tên muốn xoá: ");
        string name = Console.ReadLine() ?? string.Empty;
        employeeManagement.RemoveEmployee(name);
    }
}
