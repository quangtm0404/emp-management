﻿Console.OutputEncoding = System.Text.Encoding.UTF8;

EmployeeProgram program = new EmployeeProgram();
program.Run();

class EmployeeProgram
{
    public void Run()
    {
        // đối tượng này chỉ dùng để làm giao diện nhận/gửi dữ liệu
        EmployeeManagementUI employeeManagementUI = new EmployeeManagementUI();
        string choice = string.Empty;
        while (choice != "6")
        {
            Console.WriteLine("------ Menu Chức Năng ------");
            Console.WriteLine("1. Xem thông tin nhân viên");
            Console.WriteLine("2. Thêm nhân viên mới");
            Console.WriteLine("3. Xóa nhân viên");
            Console.WriteLine("4. Lưu vào File");
            Console.WriteLine("5. Đọc từ File");
            Console.WriteLine("6. Thoát");

            Console.Write("Lựa chọn của bạn: ");
            choice = Console.ReadLine() ?? string.Empty;

            switch (choice)
            {
                case "1":
                    employeeManagementUI.PrintEmployees();
                    break;

                case "2":
                    employeeManagementUI.AddEmployee();
                    break;

                case "3":
                    employeeManagementUI.RemoveEmployee();
                    break;
                case "4":
                    employeeManagementUI.SaveToFile();
                    break;
                case "5":
                    employeeManagementUI.ReadFromFile();
                    break;
                case "6":
                    break;
                default:
                    Console.WriteLine("Nhập không hợp lệ");
                    break;
            }
        }
        Console.WriteLine("Chương trình kết thúc");
    }
}