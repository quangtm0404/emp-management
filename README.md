# Quản lý Nhân Viên

## Lớp `Employee`

Lớp `Employee` có các thuộc tính sau:
- `Name` (string): Tên của nhân viên.
- `Age` (int): Tuổi của nhân viên.
- `Position` (string): Chức vụ của nhân viên.
- `Salary` (float): Lương của nhân viên.

## Lớp `EmployeeManagement`

Lớp `EmployeeManagement` được thiết kế để quản lý danh sách các nhân viên.

### Thuộc tính
- `employees` (mảng): Mảng chứa danh sách các nhân viên, có thể chứa tối đa 100 phần tử.
  
### Phương thức

#### `PrintEmployees`
- Mô tả: In ra thông tin của tất cả các nhân viên ra màn hình console.
- Định dạng in ra: `Tên: [name], Tuổi: [age], Chức vụ: [position], Lương: [salary]`.

#### `AddEmployee`
- Mô tả: Thêm một nhân viên mới vào danh sách.
- Yêu cầu người dùng nhập tên, tuổi, chức vụ và lương của nhân viên mới.
- Tạo một đối tượng `Employee` và thêm vào danh sách.
  
#### `RemoveEmployee`
- Mô tả: Loại bỏ một nhân viên khỏi danh sách.
- Yêu cầu người dùng nhập tên của nhân viên cần xóa.
- Tìm và loại bỏ nhân viên có tên trùng khớp từ danh sách.

## Chương Trình Chính

Khởi tạo một đối tượng của lớp `EmployeeManagement`.

Hiển thị một menu chức năng với các lựa chọn sau. Sau khi chọn chức năng, gọi hàm xử lý tương ứng trong lớp `EmployeeManagement`:

### Menu Chức Năng:
1. Xem thông tin nhân viên
2. Thêm nhân viên mới
3. Xóa nhân viên
4. Thoát
